/*
* This class represents a four sided figure [Square and Rectangle]
* */

export default class Rectangle {
  constructor(length, breadth) {
    this.length = length;
    this.breadth = breadth;
  }

  area() {
    return this.length * this.breadth;
  }

  perimeter() {
    return 2 * (this.length + this.breadth);
  }

  static createRectangle(length, breadth) {
    return new Rectangle(length, breadth);
  }

  static createSquare(side) {
    return new Rectangle(side, side);
  }

}
