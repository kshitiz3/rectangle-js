import Rectangle from "../src/Rectangle.js";

describe("Rectangle", () => { //nouns or pronouns in the
                              // outer module
  describe("Area", () => {
    test("should return 4 as area when length is 2 and breadth is 2", () => {
      let rectangle = Rectangle.createRectangle(2, 2)
      expect(rectangle.area()).toBe(4);
    });

    test("should return 144.0 as area when length is 12.0 and breadth is 12.0",
        () => {
          let rectangle = Rectangle.createRectangle(12, 13)
          expect(rectangle.area()).toBe(156.0)
        })
  })
  describe("Perimeter", () => {
    test("should return 6 as perimeter when length is 2 and breadth is 3",
        () => {
          let rectangle = Rectangle.createRectangle(2, 3);
          expect(rectangle.perimeter()).toBe(10);
        })

    test("should return 8 as perimeter when side is 2", () => {
      let square = Rectangle.createSquare(2);
      expect(square.perimeter()).toBe(8);
    })
  })
})
